<?php
/**
 * Copyright (c) 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");

$Nav = new Nav();
$Nav->addNavSeparator("Development Program", "/contribute/dev_program.php");
$Nav->addCustomNav("Work items", "https://projects.eclipse.org/development-efforts", "", 1);
$Nav->addCustomNav("FAQ", "/contribute/dev_program/faq.php", "", 1);
$Theme->setNav($Nav);
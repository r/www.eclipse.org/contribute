<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Christopher Guindon (Eclipse Foundation) - Initial implementation
 * Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<h1><?php print $pageTitle; ?></h1>
<small><b>Draft</b> v05 - September 17th, 2015</small>
<h2>Program Objective:</h2>
<p>The Friend of Eclipse Enhancement Program, or FEEP, utilizes the funds donated through the
  Friends of Eclipse program to make significant and meaningful improvements and enhancements to the
  Eclipse IDE/Platform.</p>
<h2>Program Summary:</h2>
<p>The Eclipse Foundation (EF) is committed to directing the funds donated through the Friends of
  Eclipse initiative directly to making improvements to the Eclipse IDE/Platform.</p>
<p>EF will engage with key stakeholders in the community to determine the highest priority issues to
  be addressed, convert these priorities into actionable development tasks, and then engage with
  qualified resources to carry out these tasks.</p>
<p>The guiding principles of the process are:</p>
<ul>
  <li>To adhere to the principle of transparency and openness.</li>
  <li>To complement the existing development efforts already underway.</li>
  <li>To encourage a “multiplying effect” where community participation is amplified by this funding
    program’s undertakings.</li>
</ul>
<p>Development will be carried out in an on-going series of Development Cycles, which are described
  below.</p>
<p>The effectiveness of the Program is dependent on the funds available. The expectation is for
  contributions to continue to grow, especially once positive results are demonstrated.</p>
<blockquote>NOTE: FEEP utilizes the funds donated exclusively through the Friends of Eclipse
  program. EF also accepts corporate donations, including directed donations. In the case of
  directed donations, a corporation may request their donation be directed to priorities identified
  by them. EF will ensure these donations are used for these exclusive purposes, regardless of
  whether these corporate priorities differ from the priorities identified by FEEP. However, EF is
  motivated to ensure the impact of the donations through both sources collectively, regardless of
  corporate or individual donations, leads to the maximum positive impact.</blockquote>
<h2>Program Governance:</h2>
<p>FEEP will be managed exclusively by the Eclipse Foundation. The EF commits to a full disclosure
  of the expenditures made related to this program, as well as disclosure of the priorities
  established for selecting development efforts.</p>
<p>The Eclipse Management Organization (EMO) will designate a FEEP Program Manager (“FEEP PM”) to
  manage the various tasks and processes on its behalf. Initially, it is expected the role of FEEP
  PM will be played by one of EF’s staff member, though this may change over time.</p>
<p>As the Program grows, EF may decide to create an IDE/Platform Working Group. This would be done
  primarily to broaden governance responsibilities to interested Members, and would establish
  committees to, for example, oversee budgets, contribute corporate funding directly to the program,
  expand the prioritization process, work more closely with contractors, etc.</p>
<h2>Budget:</h2>
<p>The amount of funds available for development will vary, depending on the amount of donations
  received through the Friends of Eclipse initiative.</p>
<p>Initially, EF will collect the funds received over the previous half year, and then use this
  amount for the development efforts identified for the upcoming quarter. For example, if we receive
  $75,000 in contributions in Q3, we will apply $75,000 to the development efforts in Q4. EF may
  change this to a more or less frequent process, depending on the experience gained.</p>
<p>As background, in 2014 the amount donated through the Friends of Eclipse totaled $120,000.</p>
<h2>Expected Areas of Focus:</h2>
<p>Initially, the primary areas of focus to be addressed by this Program are:</p>
<ul>
  <li>Resolve deep technical issues that are outstanding and not being addressed by the community
    otherwise.</li>
  <li>Accelerate work on areas deemed critical by key stakeholders, including the Architecture
    Council, relevant PMCs and others, to aid in keeping the Eclipse IDE state of the art.
    “Accelerate work” can take on many forms, from directly funding development to reviewing inbound
    community contributions.</li>
  <li>Make improvements to the IDE that make adoption within the community simpler and broader.</li>
  <li>Improve the tools, processes, etc. related to directly improving the IDE.</li>
</ul>
<p>EF reserves the right, through its governance role, to modify these areas of focus.</p>
<h2>FEEP Cycles:</h2>
<p>To create a process that is easier to track, budget for, and measure success, development will be
  done through a series of FEEP Cycles.</p>
<p>At a glance, each FEEP Cycle involves a 5 step process:</p>
<table class="table table-bordered">
  <thead>
    <tr>
      <th><strong>EF FEEP Cycle Activity</strong></th>
      <th><strong>Estimated Time Period</strong></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>Collect Input on Priorities</td>
      <td>Weeks 1-2</td>
    </tr>
    <tr>
      <td>Establish Budget and Select Approved Development Efforts</td>
      <td>Week 2-3</td>
    </tr>
    <tr>
      <td>Solicit Bids For Development Efforts</td>
      <td>Week 3-4</td>
    </tr>
    <tr>
      <td>Award Contracts For Development Efforts</td>
      <td>Week 5</td>
    </tr>
    <tr>
      <td>Sign-Off On Completed Development Efforts</td>
      <td>Week 13 [1]</td>
    </tr>
  </tbody>
</table>
<p>Each step in the FEEP Cycle process is described below.</p>
<p>It is expected that FEEP Cycles will be done initially on a semi-annual basis, though the EF may
  change the frequency at its sole discretion.</p>
<h3>1. Collect Input on Priorities</h3>
<p>The FEEP-PM will gather input from interested parties, including:</p>
<ul>
  <li>Respective PMC’s</li>
  <li>Member corporations that have donated funds with specific requests for development.</li>
  <li>The Eclipse Management Organization (EMO), which includes</li>
  <ul>
    <li>Architecture Council</li>
    <li>Planning Council</li>
    <li>Foundation staff</li>
  </ul>
</ul>
<p>From this input and interaction, the FEEP-PM will produce a collection of Development Efforts.</p>
<p>The description of each Development Effort is expected to include, as a minimum:</p>
<ul>
  <li>task name and Bugzilla id,</li>
  <li>time estimate,</li>
  <li>cost estimate,</li>
  <li>expected deliverable(s)/outcome(s),</li>
  <li>anticipated dependencies,</li>
  <li>means of demonstration and/or verification of completion.</li>
</ul>
<p>The FEEP-PM is likely to engage with potential contractors at this stage to seek their input on
  the Development Efforts to enable him/her to provide better estimates. This input from potential
  contractors will be provided based strictly on a non-committal basis by both sides, and is used
  for informational purposes only. No funding will be provided to potential contractors for this
  input.</p>
<h3>2. Establish Budget and Select Approved Development Efforts</h3>
<p>The FEEP-PM will create a draft budget identifying which of the Development Efforts identified
  will go to contract. This draft budget will be based on the funds available through donation, as
  well as the relative priorities established.</p>
<p>The EMO will review both the budget and list of prioritized Development Efforts, and will
  generate a list of Approved Development Efforts.</p>
<blockquote>Note: Any budgeted development funds that are not allocated for development, or any
  funds not released due to non-completion of a development effort, will be added to the funds
  available for the next Development Cycle.</blockquote>
<p>Before soliciting bids for the Approved Development Efforts, the FEEP-PM will review with the
  affected project or PMC the Approved Development Efforts targeted in their project. The project or
  PMC must commit their support to the proposed work, including naming the project committer(s) that
  will review, and commit the work if the contractor isn't a committer on that project.</p>
<h3>3. Solicit Bids for Approved Development Efforts</h3>
<p>The FEEP-PM will post the Approved Development Efforts to the EF website. During the open bidding
  period, expected to be approximately 2 weeks, potential contractors may interact with the FEEP-PM
  to seek additional information regarding the development efforts.</p>
<p>Interested contractors may submit confidential bids for any or all of the Approved Development
  Efforts.</p>
<blockquote>Note: EMO reserves the right to solicit bids from specific Committers and/or contractors
  should it feel particular expertise or experience is required to be successful. The EMO is not
  obliged to disclose that it has chosen to carry out this solicitation.</blockquote>
<p>In each bid, the contractor must identify, as a minimum:</p>
<ul>
  <li>Description of the approa ch to address the Development Effort.</li>
  <li>Identification of who will perform the work (i.e., named developers).</li>
  <li>The proposed timeframe for the development effort.</li>
  <li>Any dependencies/caveats that exist that must be addressed.</li>
  <li>Proposed outcome for the work, including how to demonstrate the work has been completed.</li>
  <li>Explicit list of deliverables, including any interim deliverables.</li>
  <li>A proposed price, including whether the bid is being made on a fixed price or time and
    materials basis, and whether any interim milestones and associated and/or interim payments are
    proposed.</li>
</ul>
<p>All bids should indicate any taxes to be charged in addition to the costs identified. It is
  expected that no expenses shall be incurred as part of any bid, though exceptions may be proposed
  by contractors. These exceptions must be clearly identified, and contractors may only bill for
  such expenses if agreed to in writing by the FEEP-PM as part of the contract for the Approved
  Development Effort, and if the expense is actually incurred.</p>
<p>All bids submitted will be treated as confidential by EF. However, all contractors must agree to
  EF posting the name and value of each contracted Development Effort.</p>
<h3>4. Award Contracts For Successful Bids</h3>
<p>The FEEP-PM will work with others within the EMO to evaluate the submitted bids. Winning bids
  will be selected upon the following criteria:</p>
<ul>
  <li>Price</li>
  <li>Quality of proposed development and deliverables</li>
  <li>Skillset of proposed developers, with preference given to Committers in the relevant area.</li>
  <li>Bidder’s relationship with EF, with preference given to either Solution Members with
    Committers, or self-employed Committers with relevant expertise</li>
  <li>Any additional relevant elements in the bid, including delivery date, whether fixed price vs.
    time and materials basis, etc.</li>
</ul>
<p>EMO may decide, at its sole discretion, not to let a contract for any particular Approved
  Development Effort, even if acceptable bids have been submitted.</p>
<p>Contractors will be expected to execute a Statement of Work (SOW) for each winning bid. Each SOW
  will be created against the standing Professional Services Agreement with the contractor, which
  must have been previously executed by the contractor.</p>
<p>Following the execution of each SOW, EMO will publish the name of the winning contractor and the
  total contract value.</p>
<h3>5. Sign-Off on Completed Development Efforts</h3>
<p>For each Development Effort, the contractor is expected to provide:
<ul>
  <li>Source code. In most cases, it will be expected that the acceptance criteria for code is that
    it has been committed by the Eclipse project. Other scenarios may be possible in exceptional
    circumstances.</li>
  <li>Test harness(es) for code</li>
  <li>Documentation, including appropriate documentation related to IP</li>
  <li>Any other appropriate deliverables, including those to which the contractor committed in the
    original bid.</li>
</ul>
<p>Unless explicitly agreed to otherwise, payment will only be made at the completion of the
  Development Effort, and once the FEEP-PM has signed-off on the deliverables. The FEEP-PM may seek
  assistance and opinions from the relevant interested parties to evaluate the completeness of the
  deliverables.</p>
<p>Sign-off by FEEP-PM on a submitted completed Development Effort is expected to take up to 14
  days.</p>
<small>[1] Completion dates may vary, depending on the individual development efforts. </small>

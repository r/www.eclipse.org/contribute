<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Christopher Guindon (Eclipse Foundation) - Initial implementation
 * Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<h1><?php print $pageTitle; ?></h1>
<div class="row">
  <div class="col-md-16">
    <h3>Report Bugs and Enhancements</h3>
    <ul class="fa-ul">
      <li><i class="fa fa-li fa-chevron-circle-right orange"></i>
        Download one of the latest <a href="/downloads/index-developer.php">
        Eclipse Package Milestone Builds</a> . Take it for a test drive
        before the final release and <a href="https://bugs.eclipse.org/bugs/enter_bug.cgi">
        report any bugs</a> you find.</li>
      <li><i class="fa fa-li fa-chevron-circle-right orange"></i>
      <a href="https://bugs.eclipse.org/bugs/enter_bug.cgi">Report Enhancements</a> :
      Got an idea for a killer feature? Or
        maybe something you use often could use some tweaking? Post an
        enhancement request!</li>
    </ul>
    <h3>Fix Bugs or Implement Enhancements</h3>
    <ul class="fa-ul">
      <li><i class="fa fa-li fa-chevron-circle-right orange"></i>
      <a href="http://bugs.eclipse.org/bugs/query.cgi">Is there some bug</a>
      that really bothers you? Instead of just
        reporting it, fix it. And there are
        <a href="https://bugs.eclipse.org/bugs/buglist.cgi?bug_status=NEW&bug_status=REOPENED&keywords=helpwanted%2C%20&list_id=9664295">
        many bugs marked with helpwanted</a>.
        <ul class="">
          <li><i class="fa fa-li fa-chevron-circle-right orange"></i>
            First sign an
            <a href="https://accounts.eclipse.org/user/eca">
            Eclipse Contributor Agreement</a>.</li>
          <li>To learn how the bug-fixing process works, check out the
          <a href="http://wiki.eclipse.org/Bug_Reporting_FAQ">
          bug reporting FAQ</a>.
          </li>
          <li>To learn about the lifecycle of bugzilla entries, check
            out the <a href="http://wiki.eclipse.org/Development_Resources/HOWTO/Bugzilla_Use">developmentprocess</a>.
          </li>
        </ul>

      <li><i class="fa fa-li fa-chevron-circle-right orange"></i>
        Participate in our <a href="dev_program.php">Friend of Eclipse
          Enhancements Program</a> either as a funded developer, or by
        making a <a href="/donate/">donation</a>.</li>
      <li><i class="fa fa-li fa-chevron-circle-right orange"></i>
      <a href="https://wiki.eclipse.org/Learn_About_Eclipse">Learn about Eclipse</a>.</li>
      <li>Browse our <a href="https://git.eclipse.org/c/">source code
          repositories</a>.
      </li>
    </ul>
    <h3>Become a Committer</h3>
    <ul class="fa-ul">
      <li><i class="fa fa-li fa-chevron-circle-right orange"></i> Start a
      <a href='http://wiki.eclipse.org/Development_Resources/HOWTO/Starting_A_New_Project'>
      new project</a> or become a committer on an
      <a href="/membership/become_a_member/committer.php">existing project</a>.</li>
    </ul>
    <h3>Promote Eclipse Technologies</h3>
    <ul class="fa-ul">
      <li><i class="fa fa-li fa-chevron-circle-right orange"></i> Let
        other people know that your application is <a href="/artwork">Built
          on Eclipse or Eclipse Ready</a>.</li>
      <li><i class="fa fa-li fa-chevron-circle-right orange"></i> Do you
        or your company have a success story about Eclipse? Send us a
        <a href="mailto:news@eclipse.org?subject=Success%20story%20about%20Eclipse">story</a>
        and we'll help you spread the word.</li>
      <li><i class="fa fa-li fa-chevron-circle-right orange"></i> Follow
        the Eclipse community on
        <a href="http://wiki.eclipse.org/Twitter">Twitter</a>.</li>
    </ul>
  </div>
  <div class="col-md-7 col-md-offset-1">
    <img src="/contribute/assets/public/images/friends_badge.png" alt="friend">
  </div>
</div>

<?php
/**
 * Copyright (c) 2014, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Wayne Beaton (Eclipse Foundation) - initial API and implementation
 * Paul White (Eclipse Foundation) - Page content
 * Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
?>
<h1><?php print $pageTitle; ?></h1>
<h4>How can I submit a bid for a development effort?</h4>
<p>
  Click on the link for any one of the <a href="https://projects.eclipse.org/development-efforts">development
    efforts</a>, and you will see a button allowing you to generate and submit a bid. Please note
  that you must be logged in to do so.
</p>
<h4>Must I be a committer to submit a bid? Must my company be a member to submit a bid?</h4>
<p>Technically, no – this is not mandatory. However, strong preference will be given to bids
  submitted by member companies and individual committers.</p>
<h4>Can I propose an alternative solution to what is being requested in the description of the
  Development Effort?</h4>
<p>Yes. It is our goal to ensure we have the best possible outcome from these Development Efforts,
  and if you feel we should consider an alternative approach, please feel free to propose it. You
  will, however, have to make clear the impact to scope, and please recognize if the cost you are
  proposing is greater than what was originally allocated, the proposed solution may not be
  feasible.</p>
<h4>Can I get clarification on a specific Development Effort?</h4>
<p>
  Yes. Please send email to <a href="mailto:feep@eclipse.org">feep@eclipse.org</a>. Please be aware
  that we may post updates to the specific Development Efforts based on the questions we receive.
</p>
<h4>Must my bid be a fixed price bid?</h4>
<p>No, you may propose an alternative pricing mechanism such as time and materials, but be aware
  that preference will be given to fixed price bids. If you wish to specify an alternative to fixed
  price, please indicate this clearly in your bid.</p>
<h4>Can I bid on multiple development efforts?</h4>
<p>Yes, but please submit each bid as a separate proposal. Each development effort will be managed
  through a separate Statement of Work (SOW).</p>
<h4>How should I indicate the impact to my delivery schedule if I am awarded multiple Development
  Efforts?</h4>
<p>If you are bidding on more than one development effort, please make clear the impact of the
  deliverable schedule should you be awarded multiple SOWs. This detail should be included in each
  bid impacted.</p>
<h4>How are bids qualified? Who is making the final decision regarding bids?</h4>
<p>All bids will be evaluated by the FEEP-PM in consultation with the team that helped develop the
  original list of Development Efforts. Bids will be evaluated using the criteria specified in the
  FEEP Program Description.</p>
<h4>When will I know whether my bid has won, and when can I begin working on the Development Effort?</h4>
<p>The FEEP-PM will contact you directly should your bid(s) be selected as a winning bid. You will
  be required to execute a services contract with Eclipse Foundation, if you have not already done
  so, and then to execute a work schedule to contract for the specific Development Effort.</p>

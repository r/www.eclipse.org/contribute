<?php
/**
 * Copyright (c) 2015, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 * Wayne Beaton (Eclipse Foundation) - initial API and implementation
 * Paul White (Eclipse Foundation) - Page content
 * Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */
require_once ($_SERVER ['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/breadcrumbs.class.php");

$App = new App();
$App->setOutdated();
$Theme = $App->getThemeClass();
include ($App->getProjectCommon());

$pageTitle = "FEEP Bidding Process FAQ";

// Set breadcrumb for this section
$Breadcrumb = new Breadcrumb();
$Breadcrumb->removeCrumb(2);
$Breadcrumb->addCrumb('FAQ',"",NULL);
$Theme->setBreadcrumb($Breadcrumb);

$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("eclipse, foundation, development, platform, funding, faq, feep");
$Theme->setPageAuthor("Eclipse Foundation");

// Generate the web page
ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setHtml($html);
$Theme->generatePage();
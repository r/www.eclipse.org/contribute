<?php
/**
 * Copyright (c) 2016, 2018 Eclipse Foundation.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *    Mike Milinkovich (Eclipse Foundation)
 *    Eric Poirier (Eclipse Foundation)
 *
 * SPDX-License-Identifier: EPL-2.0
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");

$App = new App();
$App->setOutdated();
$Theme = $App->getThemeClass();

include ($App->getProjectCommon());

$pageTitle = "Friend of Eclipse Enhancement Program (FEEP)";
$Theme->setPageTitle($pageTitle);
$Theme->setPageKeywords("eclipse, foundation, development, platform, funding");
$Theme->setPageAuthor("Mike Milinkovich");

// Generate the web page
ob_start();
include("content/en_" . $App->getScriptName());
$html = ob_get_clean();

$Theme->setHtml($html);
$Theme->generatePage();